package me.zhyd.braum.spring.boot;

/**
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @version 1.0
 * @since 1.8
 */
public enum BraumCacheType {
    MAP,
    REDIS;
}
