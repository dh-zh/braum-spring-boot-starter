package me.zhyd.braum.spring.boot.context;

/**
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @version 1.0
 * @since 1.8
 */
public class BraumBanner {

    public static final String INIT_VERSION = "1.0.0";

    public static final String LOGO =
            "                                                             \n" +
            "                                                             \n" +
            "    ,---,.                                            ____   \n" +
            "  ,'  .'  \\                                         ,'  , `. \n" +
            ",---.' .' |  __  ,-.                    ,--,     ,-+-,.' _ | \n" +
            "|   |  |: |,' ,'/ /|                  ,'_ /|  ,-+-. ;   , || \n" +
            ":   :  :  /'  | |' | ,--.--.     .--. |  | : ,--.'|'   |  || \n" +
            ":   |    ; |  |   ,'/       \\  ,'_ /| :  . ||   |  ,', |  |, \n" +
            "|   :     \\'  :  / .--.  .-. | |  ' | |  . .|   | /  | |--'  \n" +
            "|   |   . ||  | '   \\__\\/: . . |  | ' |  | ||   : |  | ,     \n" +
            "'   :  '; |;  : |   ,\" .--.; | :  | : ;  ; ||   : |  |/      \n" +
            "|   |  | ; |  , ;  /  /  ,.  | '  :  `--'   \\   | |`-'       \n" +
            "|   :   /   ---'  ;  :   .'   \\:  ,      .-./   ;/           \n" +
            "|   | ,'          |  ,     .-./ `--`----'   '---'            \n" +
            "`----'             `--`---'                                  \n" +
            "                                                             \n";

    public static String buildBannerText() {
        return System.getProperty("line.separator") + LOGO + " :: Braum ::        (v" + BraumVersion.getVersion(INIT_VERSION) + ")" + System.getProperty("line.separator");
    }
}
