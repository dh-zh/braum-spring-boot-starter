package me.zhyd.braum.spring.boot;

import javax.servlet.http.HttpServletRequest;

/**
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @version 1.0
 * @since 1.8
 */
public interface BraumProcessor {

    BraumResponse process(HttpServletRequest request);
}
