package me.zhyd.braum.spring.boot.utils;

/**
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @version 1.0
 * @since 1.8
 */
public enum GlobalBraumUtil {

    INSTANCE;

    private static final String cacheKeyPrefix = "braum_";
    private static final String blacklistKeyPrefix = "braum_blacklist_";

    public String getLockKey(String ip) {
        return formatKey(cacheKeyPrefix + ip);
    }

    public String getBlacklistKey(String ip) {
        return formatKey(blacklistKeyPrefix + ip);
    }

    public String formatKey(String key) {
        if (null == key || key.isEmpty()) {
            return null;
        }
        return key.replaceAll("[.:]", "_");
    }

}
